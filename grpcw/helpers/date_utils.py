import time, datetime

FACTOR_OF = 1000;

def stamp_from_date(value):
    if not isinstance(value, datetime.date):
        return None
    return int(time.mktime(value.timetuple()) * FACTOR_OF)

def stamp_from_datetime(value):
    if not isinstance(value, datetime.datetime):
        return None
    return int(time.mktime(value.timetuple()) * FACTOR_OF) + (value.microsecond // FACTOR_OF)

def datetime_from_stamp(input):
    if not input:
        return datetime.datetime.now()
    return datetime.datetime.fromtimestamp(input / FACTOR_OF)

def date_from_stamp(input):
    if not input:
        return datetime.date.today()
    return datetime.date.fromtimestamp(input / FACTOR_OF)

def current_stamp():
    return int(time.time() * FACTOR_OF)

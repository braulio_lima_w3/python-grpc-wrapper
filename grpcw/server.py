from concurrent import futures
import time
import threading
import os
import grpc

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class GrpcConfigurer (threading.Thread):
    def __init__(self, services=[], configs=[]):
        super().__init__()
        
        # configure server
        self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=int(os.getenv('GRPC_MAX_THREADS'))))
        self.location = os.getenv('GRPC_HOST') + ':' + os.getenv('GRPC_PORT')
        self.server.add_insecure_port(self.location)

        # logical functions
        self.startable = True
        self.maintain = True

        # further configuration of the server
        if configs and len(configs):
            for config in configs:
                if(callable(config)):
                    config(server=self.server);

        # assign services
        if services and len(services):
            for service in services:
                print('Executing', service)
                service(self.server)

    def start(self):
        self.maintain = False
        print('Starting gRPC server on a different thread')
        super(GrpcConfigurer, self).start()

    def run(self):
        if self.server and self.startable:
            self.startable = False
            self.server.start()
            print('Started gRPC server at ' + self.location)
            if self.maintain:
                try:
                    while True:
                        time.sleep(_ONE_DAY_IN_SECONDS)
                except KeyboardInterrupt:
                    self.stop()

    def stop(self):
        if(self.server):
            self.server.stop(0)
            print('Stopped gRPC server')
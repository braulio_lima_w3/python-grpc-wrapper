packages = []

try:
    from setuptools import setup, find_packages
    packages = find_packages()
except ImportError:
    from distutils.core import setup
    packages = [ 'grpcw' ]

setup(
    name='grpc-wrapper',
    version='1.0.0',
    description='A simple GRPC wrapper',
    long_description='A grpc wrapper which includes all of the basic things to get a GRPC server up and running.',
    packages=packages,
    classifiers = [
        'Environment :: Web Environment',
        'Intended Audience :: Developers'
    ],
    install_requires = [
        'grpcio'
    ]
)